const BillingCycle = require('./billingCycle')
const _            = require("lodash")
BillingCycle.methods(
	['get', 'post', 'put', 'delete']
)

BillingCycle.updateOptions(
	{
		new: true, runValidators: true
	}
)

BillingCycle
	.after('post', sendErrorsOrNext)
	.after('put', sendErrorsOrNext)

function sendErrorsOrNext(req, resp, next) {
	const bundle = res.locals.bundle
	if (bundle.errors) {
		let errors = parseErrors(bundle.errors)
		resp.status(500).json({errors})
	} else {
		next()
	}
}

function parseErrors(nodeRestfulErrors) {
	const errors = []
	_.forIn(nodeRestfulErrors, error => errors.push(error.message))
}

BillingCycle.route('count', function (req, resp, next) {
		BillingCycle.count(function (error, value) {
				if (error) {
					resp.status(500).json({errors: [error]})
				} else {
					resp.json({value})
				}
			}
		)
	}
)
module.exports = BillingCycle